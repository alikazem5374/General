# New Country PEO Request

If you wish to hire a team member in a country where we currently do not have a PEO or an entity. Please complete the details below & assign to the Employment Solutions Specialist (@rhendrick)

Please allow up to **3 - 5 weeks** for setup. A benefits review may take longer. 

## Recruitment: 
- [ ] Country: 
- [ ] Proposed hire date:

## Employment Solutions: 
- [ ] Reach out to the PEO for a quote & country exhibt
- [ ] Review with wider international expansion team
- [ ] Open a benefits issue
- [ ] Request approval from Director of Tax
- [ ] Request singature from CFO
- [ ] Update the handbook to reflect this change
- [ ] Ping recruiter when process is complete

## Recruitment & CES: 
- [ ] Send candidate information to PEO for contractor & onboarding
