# Onboading Buddy Quarterly Winner 
https://about.gitlab.com/handbook/general-onboarding/onboarding-buddies/#procedures

## Task List 
- [ ] Pull the [OSAT](https://docs.google.com/spreadsheets/d/1sAaQntIaQAnj8Z1NY6WRyQGRIyIoKa_6TratKWtScdo/edit#gid=787754436 Results)
- [ ] Pull the [Career Mobility Survey Results](https://docs.google.com/spreadsheets/d/1rxrtgxZUrSVHwBj3ZGtn8UUDV32juBknlx6BKQBjHTE/edit#gid=1057162945)
- [ ] Review all onboarding buddy names and career mobility buddy names, ensuring all names are correct
- [ ] Add all buddy names or numbers to https://wheelofnames.com/#, it will support 200 names 
- [ ] Review script and make any adjustments or changes 
- [ ] Record yourself spinning the wheel of names for the winners, remember to thank all buddies who participated 
- [ ] Email team members the Printfection link as per the steps in the [Handbook](https://about.gitlab.com/handbook/general-onboarding/onboarding-buddies/#procedures)



- [ ] Update handbook with any changes
- [ ] Consider future iterations of onboarding buddy program 

/label ~"Workflow::In Progress" ~"PEA Team"
