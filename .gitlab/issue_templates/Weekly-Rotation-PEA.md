# This Template is used to track and allocate tasks within the People Experience Team. 

On a bi-weekly basis this template will be created, assigning the relevant People Experience Associates to the specific rotation. This will also be used to update as tasks are completed, handover of certain items and will be assigned to all Associates. 

# Rotations

## Verification of Employment/Probation Period/Triage Issues/Onboarding Buddy Raffle

### Assigned: 

##### VOE 
- [ ] Respond to any Verification of Employment requests received for team members.

***ensure consent is received by the applicable team member before providing any personal information.***

##### [Triage Issues](https://about.gitlab.com/handbook/people-group/people-experience-team/#triage-people-group-issues) 
- [ ] Week 1 - Review and update labels within People issue tracker
- [ ] Week 2 - Review and update labels within People issue tracker

##### [Probation Emails](https://about.gitlab.com/handbook/people-group/people-experience-team/#probation-period-rotation)
- [ ] Update tracker of probation periods falling during this week
- [ ] Ensure the email is automatically being sent to manager

##### Bonus Processing 
- [ ] Review and [process all bonus nominations](https://about.gitlab.com/handbook/incentives/#nominator-bot-process) that come through the private `#people-group-nominator-bot` slack channel

##### [Onboarding Buddy Raffle](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-buddies/#procedures)
- [ ] Follow the relevant procedure for the onboarding buddy raffle 2 weeks before the [current quarter ends](https://about.gitlab.com/handbook/finance/#fiscal-year). 

## Allocations/Job Information Changes 

### Assigned: 

##### Allocations 
Update Onboarding Tracker - Plese be sure to allocate onboardings 2 weeks out

1. Week one:
    - [ ] Monday
    - [ ] Wednesday 
1. Week two:
    - [ ] Monday
    - [ ] Wednesday 

- [ ] Update Offboarding
- [ ] Update Career Mobility 

##### Information Changes
- [ ] Review and process all information changes through BambooHR for team members. 
- [ ] Process any job information changes that come to the Experience inbox

## Invites and Reports 

### Assigned: 

##### Anniversary/New Hire Swag
- [ ] The Associate in this rotation will send the monthly anniversary gift emails on the last working day of the specific month (alternatively first of the following month) as per this [process](https://about.gitlab.com/handbook/people-group/people-experience-team/#anniversary-period-gift-queries).
- [ ] The Associate in this rotation will pull the new hire [Printfection report](https://about.gitlab.com/handbook/people-group/people-experience-team/) on Friday's
   - [ ] Week 1
   - [ ] Week 2
   
##### Send Moo Invite
- [ ] Send [Moo Invites](https://about.gitlab.com/handbook/people-group/people-experience-team/) on Tuesday of the current week  
   - [ ] Week 1
   - [ ] Week 2

##### Send TaNEWki Call Invites
- [ ] If applicable, send invitation to new hires for the Ta'NEW'ki welcome call 2 weeks before their start date. 

##### Missing Emergency Contact Emails
- [ ] The People Experience inbox will get an email titled `Sisense SQL Alert: Missing BambooHR Emergency Contact!` - using the [email template](https://gitlab.com/gitlab-com/people-group/people-operations/General/-/tree/master/.gitlab/email_templates), email all the recipients (bcc) to urge them to fill in the information

##### Social Call Metrics
- [ ] First week of the Month create a new issue and [Pull Social Call Metrics](https://about.gitlab.com/handbook/people-group/people-experience-team/#pulling-social-call-metrics)

**Please also pull the Mental Health Social Call metrics to track each month**

##### Onboarding Cohorts
- [ ] On the last week of the month, [create a new public slack channel for the next month's onboarding cohort](https://about.gitlab.com/handbook/people-group/people-experience-team/#onboarding-cohort-creation)
- [ ] Post in the 3 month old onboarding cohort using this [template](https://gitlab.com/gitlab-com/people-group/people-operations/General/-/blob/master/.gitlab/email_templates/onboarding-cohort-slack-notification) a few days before the last day of the month
- [ ] Archive the 3 month old onboarding cohort slack channel on the last day of the month

##### I-9 Purging
- [ ] The first week of the month, the PEA will complete the [I-9 purging process](https://about.gitlab.com/handbook/people-group/people-experience-team/#retaining--purging-form-i-9) for the previous month.


##### Close out the issue 

 Close this issue and open next 

## Additional Projects

Please use this section to update with any additional project information 


/label ~"PEA Team" ~"Workflow::In Progress"
