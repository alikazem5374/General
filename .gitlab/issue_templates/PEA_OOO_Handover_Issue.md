# This issue is for the People Experience Associate when they are OOO
Please be sure to title issue:

- Name, OOO-(month/day- month/day) 

### Onboarding Issue
- [ ] PEA 1
    - list the issue
- [ ] PEA 2
    - list the issues

### Offboarding Issue
- [ ] PEA 1
    - list the issue
- [ ] PEA 2
    - list the issues

### Career Mobility
- [ ] PEA 1
    - list the issue
- [ ] PEA 2
    - list the issues

### Rotational Tasks
- Please tag the approriate PEA to do the tasks in the current weeks issue

### Issue's to move forward 
- Please be sure to list the issues that need PEA attention


/label ~"PEA OOO" ~"PEA Team"
