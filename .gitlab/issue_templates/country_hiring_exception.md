This issue template is to be completed by a hiring manager if they wish to seek an exception to our country hiring guidelines. At present we are focussing our hiring in countries where we have [entities](https://about.gitlab.com/handbook/people-group/employment-solutions/#gitlab-entities) & [PEO's](https://about.gitlab.com/handbook/people-group/employment-solutions/#peo-professional-employer-organization-employer-of-record-and-not-a-gitlab-entity). Hiring outside of a PEO or Entity is not allowed without an approved exception.

***Hiring Manager to complete below***

* [ ] Mark issue confidential
* [ ] Name: (Text)
* [ ] Location: (Text)

**What role is the candidate applying for?**

* [ ] (Text)

**How was the candidate sourced?**

* [ ] (Text)

**When did the candidate apply?**

* [ ] (Text)

**How long has the req for the role been open?**

* [ ] (Text)

**What is the business need to hire this candidate outside of an open for hiring location?**

* [ ] (Text)

**What is the risk by not hiring this particular candidate?**

* [ ] (Text)

**Were there other qualified candidates considered?**

* [ ] (Text)
* [ ] Assign to @hdevlin once the above is complete

***Employment Solutions to complete below***

**Exception Type**

* [ ] Hire in a location where we currently have no team members
* [ ] Hire in a location that is blocked for hiring with contractor team members
* [ ] Hire in a location that is blocked for hiring with a PEO

**Requested location status**

* [ ] (Text)

**Associated Concerns / Barriers to entry**

* [ ] (Text)

**Recommendation**

* [ ] (Text)

***E/C Level Approval***

* [ ] Employment Solutions: Ping E/C level team member for overview / approval
