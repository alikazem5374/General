# Quarterly Audits 

This issue template is used by the People Experience team to track and collaborate on audits that happen on a quarterly basis. 

### Timing 
This issue should be opened the first week of a new quarter with a due date of the end of the month. 

## Code of Conduct 
- [ ] The Associate in the specific rotation will pull a report from BambooHR for `Code of Ethics Acknowledgment`  and 'Relocation Acknowledgement' to check that all pending team member signatures have been completed. 
- [ ] If it has not been signed by the team member, please select the option in BambooHR to send a reminder to the team member to sign. Please also follow up via Slack and ask the team member to sign accordingly. 
- If there are any issues, please escalate to the People Experience Team Lead for further guidance.

## Anniversary Gift Inventory 
- [ ] Review sock inventory and if the stock is low, we will proceed with placing an order for new stock to the warehouse.
- [ ] Review vest inventory
- [ ] Review bag inventory
      - Process for the PEA to follow is listed in the [Handbook](https://about.gitlab.com/handbook/people-group/people-experience-team/#audits--quarterly-rotations). 

## Onboarding Audit
The first quarter of the year (February 1 to April 30)
- [ ] PEA team will need to perfom an audit on the tasks in this issue.

      - ensure compliance pieces are up-to-date
      - add any additional tasks based on OSAT feedback
      - remove any tasks that are unnecessary 
      - go through open issues to close/reach out

## Offboarding Audit
The second quarter of the year (May 1- July 31)
- [ ] PEA team will need to perform an audit on the tasks in this issue.

     - ensure de-provisioners are correctly listed
     - ensure systems are up-to-date
     - ensure tasks PEA tasks are up-to-date
     - remove any tasks that are unnecessary
     - go through open issues to close/reach out

## Career Mobility Audit
The third quarter of the year (August 1- October 31)
- [ ] PEA team will need to perform an audit on the tasks in this issue

      - ensure compliance pieces are up-to-date
      - review and if applicable, apply, feedback from the career mobility survey
      - remove any tasks that are unnecessary
      - go through open issue to close/reach out

/label ~"PEA Team" ~"Workflow::In Progress"
