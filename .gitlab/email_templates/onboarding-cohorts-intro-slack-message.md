Hi everyone! :wave:

You have been automatically added to this onboarding lounge. This channel will include all team members starting this month. 

This is YOUR space to connect, ask questions, get to know others as well as getting familiar with everything GitLab has to offer :smile:

There will be automatic prompts that will be shared by the Donut Watercooler app, these are a great way to get to know each other, as well as an opportunity to ask anything about onboarding. 

We also have a [TaNewKi Tips page](https://about.gitlab.com/handbook/people-group/general-onboarding/tanewki-tips/) that could be helpful during your onboarding period.

Please do not feel like you can never ask questions, we are always here to help, guide and laugh! And also remember we are all here to help set you up for success! 

People Experience Team
