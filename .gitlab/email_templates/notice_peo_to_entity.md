Hi {VendorName},

Hope you are safe and all well on your side? 

We have been able to establish an entity and also a bank account in {Country}, due to the scaling we've seen there and the future scaling we are planning. 

We are hereby giving notice to {Vendor} and will be converting our team members on {ConversionDate}. Kindly confirm if this date will work for you? 
Please let me know if you have any additional questions and need to sync with any of our team to ensure a fluid hand-off. 

We appreciate all the support during the last few years and thankful for the partnership you gave to GitLab! 

Best regards
{People Operations Specialist}
