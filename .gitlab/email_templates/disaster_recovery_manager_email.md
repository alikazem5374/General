Subject: [`Natural Disaster`] in [`Location`] 
(Remember to cc `peopleops@gitlab.com`, `peoplepartners@gitlab.com` and `people-exp@gitlab.com`)

Hello GitLab Managers

Kindly note that the People Experience Team has been advised of a {insert natural disaster} which recently took place in {insert location}.

We have reached out to all team members in the impacted location and asked that they check-in / update us of their status when possible.

Those impacted may be unavailable or offline for an undisclosed period of time. Should a team member reach out for support, we ask that you escalate this to a member of the People Experience Team at your earliest convenience.

{Insert Communication sent to team members}

