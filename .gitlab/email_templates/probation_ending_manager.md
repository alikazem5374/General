Dear `__MANAGER_NAME__`,

`__TEAM_MEMBER_NAME__`'s probation period will be ending on `__PROBATION_END_DATE__` as per the BambooHR. If you could kindly confirm whether you are satisfied with their performance.  If you are satisfied please schedule a <a href='https://about.gitlab.com/handbook/leadership/1-1/'>121</a> with your direct report before the probation end date to confirm succesful completion.
If you could kindly inform us of the meeting date and we will prepare a confirmation letter.

More information on probation periods can be found on <a href='https://about.gitlab.com/handbook/people-group/contracts-probation-periods/'>this page</a> within the GitLab handbook.

If there are any performance concerns (results, behaviors, etc) and/or you are not sure if you should approve or extend the probation for your new hire, the manager should engage their PBP immediately. Please note that in some countries, the ability to extend probation is subject to local laws and country requirements.

Thank you,

People Experience Associate Team
