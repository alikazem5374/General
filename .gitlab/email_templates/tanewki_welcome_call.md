Subject: Join us for a GitLab welcome call 


Hi New Team Members,

As we count down to your first day at GitLab, we’d like to invite you to a pre-onboarding “Ta-New-Ki” welcome call!

This is a great opportunity to meet other new team members, get helpful tips for onboarding, and ask our team any lingering questions ahead of your start date. At GitLab, we call this type of Q&A session an “Ask Me Anything” (AMA), so any and all questions are welcomed. You can view more about the call and Onboarding here (https://about.gitlab.com/handbook/people-group/general-onboarding/).

We also have a [Ta-New-Ki Tips handbook page](https://about.gitlab.com/handbook/people-group/general-onboarding/tanewki-tips/) that has been created based on questions we've received from previous new hires. This is a great resource that will help you as well.

The call is completely optional, but we’d love to see you there! 

Planning to join us? Sign up here (https://forms.gle/FL6ozGdsoMMik6JW6). The Zoom link and agenda for the call are listed below. 

{Add the zoom link from the calendar }

Please let us know if you have questions. 

All the best,
{YOUR NAME}
{YOUR TITLE} 
