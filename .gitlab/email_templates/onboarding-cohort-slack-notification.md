:wave: all -

We hope that this slack channel has been helpful for the first 3 months of your employment here in creating connections and having a resource of those going through similar experiences as you. On the last day of this month, we are going to archive this channel but don't let that be a stopper of keeping these relationships going. Also remember, there are tons of other social channels to join here at GitLab #dog, #cat, #gaming, #fitlab, and many more! 

The People Experience Team
