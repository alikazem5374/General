Hello [Team Member], 

During our emergency contact audit, you have been identified as someone who does not have an Emergency Contact within BambooHR. Adding an Emergency Contact allows People Business Partners or Executives be able to know who to contact in the rare case something happens while on a Zoom call. 

You can update your Emergency contact by going into BambooHR, clicking on the Emergency tab, and filling in the information. 

If you have any questions or concerns, please don't hesitate to contact people-connect@gitlab.com or your relevant People Business Partner. 

Thank you!
