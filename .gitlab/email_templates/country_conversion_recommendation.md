Subject: [COUNTRY NAME] - Country Conversion Recommendation
To: `countryconversions @gitlab.com`

Hi {{Add the person's name you are addressing this to, usually our Director of Tax}}

Kindly review the recommendation for converting {{Country X}} below and confirm approval or feedback in the email thread.

[INSERT SCREENSHOT OF SUMMARY CALCULATION]

**Current Status**
- We currently have [XX] team members [contracted/employed] on [entity/vendor] contracts in [location].
- We also have more than one viable applicant in the pipeline, awaiting an offer, per Recruiting. (If applicable)
- With this approval, we would like to convert the [contractors/PEO employees] to [PEO employees/CXC self-employed/employees of an entity] on [date]], through [vendor/entity].

**Considerations**
- The ER costs are [XX%].
- The location factor is [XX].
- Statutory benefits are of high quality, but 
- We do recommend [supplemental benefit], with Total Rewards' agreement and approval.
- Link to discussion in Country Conversion Benefits Review issue.

**Business**
- [XX%] of GitLab's prospects are from this location.
- This location is at [$XX] USD ARR.
- [$XX] USD in terms of FAS5 liability.

**Compensation Recommendation**
- Total Rewards recommends [increasing/decreasing] the team members' comp by [XX%] due to [reason].

**Impact**
- The impact to team members with our recommendations is: 
[XX% increase/decrease] in take-home pay due to [reason].
- The income tax rate for self-employed is [XX%].
- The income tax rate for employees is [XX%].
- By converting and opening up this country we will see positive growth and cost-effective hiring.

Link to complete calculations

Best regards
{{Sender}}
