Instructions: People Operations Specialist or Manager, People Operations to get the code for each individual anniversary gift from the Anniversary Gift order sheet (not linked to protect the codes) and replace XXX with the code. 
Keep in mind, some of the Annviersary gifts are retro, don't forget to add the word `Retro` in front of the subject line, if it is a gift for a past anniversary. 



Subject: Anniversary Gift

Hi PERSON,

Congrats on your anniversary at GitLab! We are so excited to have you as part of our team and want to celebrate your anniversary with you, by giving you a gift! 

Kindly click on the link to access the portal and add your personalized code XXX to access your gift. 
The Portal will request your personal information and address (if you will not be at your main residence, please ensure you place your order to be delivered at a location and time whereby you are available to take delivery). People Operations cannot send this to anyone outside of GitLab on your behalf. 

At checkout, use the code XXX which will pay for your order. Shipping is free. 
Please remember to use your GitLab email at checkout on the contact information page.

If your chosen item or size is out of stock, please reach out to peopleops@gitlab.com to assist. 
Please let me know if you have any questions!

P.S. If you received this email and your anniversary is in the past, we are retro'ing! 

Best regards,